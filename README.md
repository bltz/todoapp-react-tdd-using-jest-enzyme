# toDoApp-React-TDD-using-Jest-Enzyme

simple to do React app - Component TDD using Jest &amp; Enzyme

from scratch : 
```
1. npm install -g create-react-app
2. create-react-app react-tdd
3. cd react-tdd
4. npm install enzyme react-addons-test-utils --save-dev
```
installation Enzyme work with react ver : 
http://airbnb.io/enzyme/docs/installation/index.html


npm test -- --verbose
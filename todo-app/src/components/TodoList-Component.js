import React, { Component } from 'react';
import Todo from './Todo-Component'

class TodoList extends Component {
  state = {
    filter: 'all'
  };

  onFilterChange = (event) => {
    this.setState({
      filter: event.target.value,
    });
  };


  render() {
    const { filter } = this.state;
    let { entries } = this.props;

    if (filter === 'done') {
      entries = entries.filter(todo => todo.done);
    }

    return (
      <div className="todo-list">
        <h3 className="todo-list__header">My Todo List</h3>
        <select onChange={this.onFilterChange} className="todo-list__filter">
          <option value="all">All</option>
          <option value="done">Done</option>
        </select>
        {entries.map(todo => (
          <Todo key={todo.id} text={todo.text} isDone={todo.done} />
        ))}
      </div>
    );
  }
}
export default TodoList;
import React from 'react';
import { shallow, configure } from 'enzyme';
import Todo from './Todo-Component';
import Adapter from 'enzyme-adapter-react-15';

configure({ adapter: new Adapter() });

it('shows todo text', () => {
  const subject = shallow(<Todo text="Attend React Conf 17" isDone={true} />);
  expect(subject.find('.todo').text()).toBe('Attend React Conf 17');
});
it('shows line-through when it is done', () => {
  const subject = shallow(<Todo text="Attend React Conf 17" isDone={true} />);
  expect(subject.find('.todo').text()).toBe('Attend React Conf 17');
  expect(subject.prop('style').textDecoration).toBe('line-through');
});
it('should not show line-through when it is not done', () => {
  const subject = shallow(<Todo text="Attend React Conf 17" isDone={false} />);
  expect(subject.find('.todo').text()).toBe('Attend React Conf 17');
  expect(subject.prop('style').textDecoration).toBe('none');
});
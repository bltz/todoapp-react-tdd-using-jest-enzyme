import React, { Component } from 'react';
import TodoList from './components/TodoList-Component';
import './App.css';
class App extends Component {
  state = {
    todos: [
      { id: 1, text: 'Hello', done: false },
      { id: 2, text: 'There', done: true },
      { id: 3, text: "It's cool, isn't it?", done: false },
    ],
  };
  render() {
    return (
      <div className="App">
        {/* Pass state.todos ke TodoList */}
        <TodoList entries={this.state.todos} />
      </div>
    );
  }
}
export default App;